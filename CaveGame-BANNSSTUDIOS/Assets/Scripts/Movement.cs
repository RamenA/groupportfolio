﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Movement : MonoBehaviour
{
    private Rigidbody _RB;
    //private CharacterController _Controller;
    private Vector3 _Upwards;
 //   private bool groundedPlayer;
    public float playerSpeed;
    public float jumpHeight;
    //public float movementspeed;
    //bool grounded = false;
    //Transform cam;

    private Vector3 vel;
    public float maxSpeed;
   
    public static bool jumped;

    void Start()
    {
        _Upwards = new Vector3(0, 1, 0);
        _RB = GetComponent<Rigidbody>();
        jumped = false;
    }

    void Update()
    {
        //Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        // _controller.Move(move * Time.deltaTime * playerSpeed);
        //if (move != Vector3.zero)
        //transform.forward = move;

        vel = _RB.velocity;
        if (vel.magnitude < maxSpeed)
        { 
            Charmovement();
        }
        Debug.Log(vel.magnitude.ToString());
        

		



    }
    

    void Charmovement()
    {
        /*float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 Charmovement = new Vector3(hor, 0f, ver) * playerSpeed * Time.deltaTime;
        transform.Translate(Charmovement, Space.World);
        */
        if (Input.GetKey(KeyCode.W))
        {
            _RB.AddForce(transform.forward * Time.deltaTime * playerSpeed);

        }
        if (Input.GetKey(KeyCode.S))
        {
            _RB.AddForce(-transform.forward * Time.deltaTime * playerSpeed);

        }
        if (Input.GetKey(KeyCode.A))
        {
            _RB.AddForce(-transform.right * Time.deltaTime * playerSpeed);

        }
        if (Input.GetKey(KeyCode.D))
        {
            _RB.AddForce(transform.right * Time.deltaTime * playerSpeed);

        }
       
        if (Input.GetKeyDown(KeyCode.Space)&& jumped == false)
        {
           _RB.AddForce(_Upwards * Time.deltaTime * jumpHeight, ForceMode.Impulse);
            jumped = true;

        }


    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == ("Grounded") && jumped == true)
        {
            jumped = false;
        }
	}
}