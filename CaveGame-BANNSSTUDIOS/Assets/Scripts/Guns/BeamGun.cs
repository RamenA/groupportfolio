﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamGun : MonoBehaviour
{
    Rigidbody _rb;
    public float recoil;
    Transform cam;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        cam = this.gameObject.transform.GetChild(0);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _rb.AddForce(-cam.transform.forward * Time.deltaTime * recoil);  //use negative forward vector for recoil
        }
    }
}
