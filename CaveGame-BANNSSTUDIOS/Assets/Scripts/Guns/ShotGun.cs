﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : MonoBehaviour
{
    Rigidbody _rb;
    public float recoil;
    Transform cam;

    int ammo;
    public int ammoCap;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        cam = this.gameObject.transform.GetChild(0);
        ammo = 2;

    }

    // Update is called once per frame
    void Update()
    {
        if (ammo > 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _rb.AddForce(-cam.transform.forward * Time.deltaTime * recoil, ForceMode.Impulse);  //use negative forward vector for recoil
                ammo -= 1;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == ("Grounded") && ammo != ammoCap)
        {
            ammo = ammoCap;
        }
    }

}
