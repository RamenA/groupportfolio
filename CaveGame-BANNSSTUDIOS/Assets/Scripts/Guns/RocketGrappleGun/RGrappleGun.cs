﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RGrappleGun : MonoBehaviour
{

    Rigidbody _rb;
    public float pullForce;
    Transform cam;

    bool canShoot;
    [SerializeField]
    GameObject rocket;
    GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        cam = this.gameObject.transform.GetChild(0);

        canShoot = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canShoot == true) // press left click for shooting rocket
        {
            Vector3 spawnpoint = new Vector3(cam.transform.position.x, cam.transform.position.y, cam.transform.position.z + 2); //offset so it doesn't spawn in player
            projectile = Instantiate(rocket, spawnpoint, cam.transform.rotation);            

            canShoot = false;
        }
        if (Input.GetMouseButton(1) && Rocket.stuck == true) // hold right click for pulling to target
        {
            Vector3 pulldir = projectile.transform.position - _rb.transform.position;

            _rb.AddForce(pulldir * Time.deltaTime * pullForce);

        }
    }
}
