﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    Rigidbody _rb;
    public static bool stuck;
    public float throwSpeed;


    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        stuck = false;
        _rb.AddForce(_rb.transform.forward * throwSpeed, ForceMode.Impulse);


    }

    // Update is called once per frame
    void Update()
    {
        if (stuck == true)
        {
            _rb.velocity = Vector3.zero;
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            _rb.useGravity = false;
            stuck = true;

        }
    }
}
