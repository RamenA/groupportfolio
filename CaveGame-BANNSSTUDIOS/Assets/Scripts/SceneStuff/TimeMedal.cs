﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TimeMedal : MonoBehaviour //attach this script to the prefab for the level goal
{
    public int bronzeSecs, silverSecs, goldSecs;
    WorldTimer worldTimer;

        
    void Start()
    {
        
        worldTimer.ValueUpdate(bronzeSecs, silverSecs, goldSecs); //match WorldTimer values here

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            //display achieve
        }
    }
}
