﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WorldTimer : MonoBehaviour
{
    public static float elapsedTime;
    public TextMeshProUGUI timerText;

    public TextMeshProUGUI bronzeDisp, silverDisp, goldDisp;

    public TextMeshProUGUI achieve;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        elapsedTime = 0;

    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;

        timerText.text = "Time: " + elapsedTime.ToString("F0"); //F0 = 0 decimal places

    }

    public void ValueUpdate(int bronze, int silver, int gold) //make values display the relevant level records
    {
        bronzeDisp.text = "Bronze: " + bronze.ToString("F0");
        silverDisp.text = "Silver: " + silver.ToString("F0");
        goldDisp.text = "Gold: " + gold.ToString("F0");

    }

}
