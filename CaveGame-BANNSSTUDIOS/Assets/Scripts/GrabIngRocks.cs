﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabIngRocks : MonoBehaviour
{
    public Transform Location;

	private void OnMouseDown()
	{
        //when holding left button of the mouse and select the object, it will go to the player
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        //above lines will set the object rigidbody and will not fly off
        this.transform.position = Location.position;
        this.transform.parent = GameObject.Find("Grab").transform;
        //this will locate the player and attact to there using gameobject

	}
    private void OnMouseUp()
    {
        //everything will be inversed once you let go of mouse. the object will be in the place where you put it.
        this.transform.parent = null;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<BoxCollider>().enabled = true;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Grab()
	{
        if(Input.GetKey(KeyCode.E))
		{
            
		}
	}
}
