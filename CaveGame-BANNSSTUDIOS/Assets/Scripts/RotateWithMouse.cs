﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWithMouse : MonoBehaviour
{
    [SerializeField]
    private float turnSpeed = 3.0f;
   // Quaternion quaternion;

    
    void Update()
    {
        float horizontal = Input.GetAxis("Mouse X");
        transform.Rotate(horizontal * turnSpeed * Vector3.up, Space.World); //Y rotation

        float vertical = Input.GetAxis("Mouse Y");
        transform.Rotate(vertical * turnSpeed * -Vector3.right, Space.World); //X rotatoin

        Quaternion quaternion = new Quaternion(transform.rotation.x, transform.rotation.y, 0, 1);
        transform.rotation = quaternion;
        //quaternion.eulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, 0);
        //transform.rotation = quaternion;

    }
}
