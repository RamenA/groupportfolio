﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    Transform cam;
    Rigidbody _rb;
    public float mouseSpeed;


    void Start()
    {
        cam = this.gameObject.transform.GetChild(0);
        _rb = GetComponent<Rigidbody>();

    }

    
    void Update()
    {
        MouseLook();

    }

    void MouseLook()
    {
        float mx = mouseSpeed * Input.GetAxis("Mouse X");
        _rb.transform.Rotate(0, mx, 0);

        float my = mouseSpeed * Input.GetAxis("Mouse Y");
        cam.transform.Rotate(-my, 0, 0);

    }
}
