﻿using UnityEngine;
using System.Collections;

public class TransformFollower : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    [SerializeField]
    private Vector3 offsetPosition;

    [SerializeField]
    private Space offsetPositionSpace = Space.Self;

    [SerializeField]
    private bool lookAt = true;

    Transform cam;

    public float mouseSpeed;

    private void Start()
    {
        cam = transform.GetChild(0);
    }
    private void Update()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (target == null)
        {
           

            return;
        }

        // compute position
        if (offsetPositionSpace == Space.Self)
        {
            transform.position = target.TransformPoint(offsetPosition);
        }
        else
        {
            transform.position = target.position + offsetPosition;
        }

        // compute rotation
        if (lookAt)
        {
            // transform.LookAt(target);
            float mx = mouseSpeed * Input.GetAxis("Mouse X");
            transform.Rotate(0, mx, 0);

            float my = mouseSpeed * Input.GetAxis("Mouse Y");
            transform.Rotate(-my, 0, 0);
            transform.rotation = new Quaternion(transform.eulerAngles.x, transform.eulerAngles.y, 0, 0);
        }
        else
        {
            transform.rotation = target.rotation;
            //transform.rotation = Input.GetAxis("Mouse X");
           
        }
    }
}